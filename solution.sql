--Create dabase
 CREATE DATABASE blog_db;
 Use blog_db;

 --Create table users;
 CREATE TABLE users(
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(300) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id)
 );

 --Create table post
 CREATE TABLE post(
     id INT NOT NULL AUTO_INCREMENT,
     title VARCHAR(500) NOT NULL,
     content VARCHAR(5000) NOT NULL,
     datetime_posted DATETIME NOT NULL,
     author_id INT NOT NULL,
     PRIMARY KEY(id),
     CONSTRAINT fk_post_user_id
        FOREIGN KEY(author_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
 );

-- Create table post_comments
CREATE TABLE post_comments(
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    users_id INT NOT NULL,
    content VARCHAR(5000),
    datetime_commented DATETIME NOT NULL,
    PRIMARY KEY(id),
        CONSTRAINT fk_post_comment_post_id
            FOREIGN KEY(post_id) REFERENCES post(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT,
        CONSTRAINT fk_post_comment_user_id
            FOREIGN KEY(users_id) REFERENCES users(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT
);

-- Create table post_likes
CREATE TABLE post_likes(
    id INT NOT NULL AUTO_INCREMENT,
    post_id INT NOT NULL,
    users_id INT NOT NULL,
    datetime_liked DATETIME NOT NULL,
    PRIMARY KEY (id),
        CONSTRAINT fk_post_likes_post_id
            FOREIGN KEY(post_id) REFERENCES post(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT,
         CONSTRAINT fk_post_likes_user_id
            FOREIGN KEY(users_id) REFERENCES users(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT

);